
<p align="center" width="100%">
    <img src="./img/policeman_256px.png">
</p>

## Understanding Intel SGX

<br>
<br>

[ Readings ] |
:---------: |
<br>

#### _*Architecture*_:
- [x] [Whitepaper HASP 2013](https://software.intel.com/sites/default/files/article/413936/hasp-2013-innovative-instructions-and-software-model-for-isolated-execution.pdf)
- [x] [Whitepaper MIT](https://eprint.iacr.org/2016/086.pdf)
- [x] [Quark's Lab](https://blog.quarkslab.com/overview-of-intel-sgx-part-1-sgx-internals.html)

<br>

#### _*Considerations*_:
- [x] [SSLab - GATech](https://sgx101.gitbook.io/sgx101/)
- [x] [Space18 Blackhat](https://github.com/jovanbulck/sgx-tutorial-space18)

<br>

#### _*Development*_:
- [x] [Intel SDK](https://software.intel.com/content/www/us/en/develop/topics/software-guard-extensions/sdk.html)
- [x] [Intel Tutorial](https://software.intel.com/content/www/us/en/develop/articles/introducing-the-intel-software-guard-extensions-tutorial-series.html)
- [x] [The Magic of Intel's SGX](https://medium.com/magicofc/the-magic-of-intels-sgx-how-to-hello-it-sec-world-fb0295d6c33b)

<br/>
<br/>


---
#### *__Development Log__*

---
<br/>

Projects are committed to a development repository where it is following the Git Flow paradigm.

<br/>


##### [ March 26th 2021 ]
> * Initial project workspace setup.
> * Began researching Intel SGX Architecture.

<br/>


##### [ March 27th 2021 ]
> * Reading tutorials and whitepapers.

<br/>

---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

Icons made by [Freepik](https://www.freepik.com/ "Freepik") from
[www.flaticon.com](https://www.flaticon.com/ "Flaticon")
is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0").

---
